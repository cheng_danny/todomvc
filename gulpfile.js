var _ = require('lodash');
var gulp = require('gulp');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var p = require('gulp-load-plugins')({
    lazy: false,
});
// config
// var browserifyConfig = {
//     basedir: '.',
//     paths: './js'
// };

// function bundle (source, bundler, mode) {
//     return gulp.src(source)
//         .pipe(p.plumber({errorHandler: browserifyError}))
//         .pipe(bundler)
//         .pipe(gulp.dest('./dist'));
// }


// // caching for next build
// var cached = {};

// function browserifyError(err) {
//   p.util.log(p.util.colors.red('Error: ' + err.message));
//   this.end();
// }

// // create browserify transform
// function createBunlder (mode) {
//     return transform(function(filename) {
//         var b;
//         if ('watch' === mode) {
//             if (cached[filename]) return cached[filename].bundle();

//             b = watchify(browserify(
//                     filename, _.extend(browserifyConfig, watchify.args, {debug: true}) ));
//             cached[filename] = b;
//         };

//         // event
//         b.on('error', browserifyError);
//         if (mode === 'watch') {
//             b.on('time', function(time){
//                 p.util.log(p.util.colors.green('Browserify'),
//                         filename,
//                         p.util.colors.blue('in ' + time + ' ms'));
//             });
//             b.on('update', function(){
//                 // on file changed, run the bundle again
//                 bundle(filename, createBundler('watch'), 'watch');
//             });
//         }

//         return b.bundle();
//     });
// }

// gulp.task('js-watch', function() {
//     return bundle('./myproject/static/js/*.js', createBunlder('watch'), 'watch');
// });

// gulp.task('js-dev', function() {
//     return plugins.bundle('./myproject/static/js/*.js', createBunlder('dev'), 'dev');
// })
// gulp.task('js-prod', function() {
//     return plugins.bundle('./myproject/static/js/*.js', createBunlder('prod'), 'prod');
// })

gulp.task('watch', function() {
    p.livereload.listen();
    gulp.watch(['myproject/**/*']).on('change', function (file) {
        p.livereload.reload();
    })
});
