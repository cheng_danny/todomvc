import models
import logging


log = logging.getLogger(__name__)


class Resource(dict):
    """docstring for Resource"""
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent


class Root(Resource):
    def __init__(self, request):
        self.request = request
        self.add_resource('todos', models.Todos)

    def add_resource(self, name, orm_class):
        self[name] = ORMContainer(name, self, self.request, orm_class)


class ORMContainer(dict):
    def __init__(self, name, parent, request, orm_class):
        self.__name__ = name
        self.__parent__ = parent
        self.request = request
        self.orm_class = orm_class

    def __getitem__(self, key):
        log.info('orm container get item %s' % key)
        try:
            key = int(key)
        except ValueError:
            raise KeyError(key)
        obj = models.DBSession.query(self.orm_class).get(key)
        if obj is None:
            raise KeyError(key)
        obj.__name__ = key
        obj.__parent__ = self
        return obj


class Todo(ORMContainer):
    def __init__(self, ref, parent):
        ORMContainer.init(self, ref, parent)

