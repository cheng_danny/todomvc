import json
from datetime import datetime
from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import class_mapper
import logging
from .models import (
    DBSession,
    Todos
    )


log = logging.getLogger(__name__)


# @ref http://stackoverflow.com/questions/23554119/convert-sqlalchemy-orm-result-to-dict
def object_to_dict(obj, found=None):
    if found is None:
        found = set()
    mapper = class_mapper(obj.__class__)
    columns = [column.key for column in mapper.columns]
    get_key_value = lambda c: (c, getattr(obj, c).isoformat()) if isinstance(getattr(obj, c), datetime) else (c, getattr(obj, c))
    out = dict(map(get_key_value, columns))
    for name, relation in mapper.relationships.items():
        if relation not in found:
            found.add(relation)
            related_obj = getattr(obj, name)
            if related_obj is not None:
                if relation.uselist:
                    out[name] = [object_to_dict(child, found) for child in related_obj]
                else:
                    out[name] = object_to_dict(related_obj, found)
    return out


@view_config(
    route_name="todos.xhr",
    renderer="json",
    xhr=True,
    # context=Todos,
    # permission="view",
    request_method="GET")
def showTodoJSONResponse(request):
    """ Show todo item """
    try:
        todo = DBSession.query(Todos).get(request.matchdict['todo'])
    except DBAPIError:
        return Response(
            body=json.dumps({'message': conn_err_msg}),
            content_type='application/json',
            status_int=400)
    return object_to_dict(todo)


@view_config(
    route_name="todos.xhr",
    renderer="json",
    xhr=True,
    # context='resource.Todo',
    request_method="DELETE")
def deleteTodoJSONResponse(request):
    """Delete todo item"""
    log.info('delete todo item')
    try:
        todo = DBSession.query(Todos).get(request.matchdict['todo'])
    except DBAPIError:
        return Response(
            body=json.dumps({'message': conn_err_msg}),
            content_type='application/json',
            status_int=400)

    DBSession.delete(todo)

    return Response(
        status='202 Accepted',
        content_type='application/json; charset=UTF-8')


@view_config(
    route_name="todos.xhr",
    renderer="json",
    xhr=True,
    request_method="PUT")
def updateTodoJSONResponse(request):
    """ Update todo item """
    try:
        todo = DBSession.query(Todos).get(request.matchdict['todo'])
    except DBAPIError:
        return Response(
            body=json.dumps({'message': conn_err_msg}),
            content_type='application/json',
            status_int=400)

    # Client request via json data
    try:
        body = request.json_body
        # log.info('title : %s' % body['title'])
        # log.info('completed : %s' % body['completed'])
        todo.title = body['title']
        todo.completed = body['completed']
    except ValueError:
        return Response(
            body=json.dumps({'message': 'No JSON object could be decoded'}),
            status='400 Bad Request',
            content_type='application/json')

    # if request.params.get('title'):
    #     todo.title = request.params.get('title')

    # if request.params.get('completed'):
    #     todo.completed = request.params.get('completed')

    return object_to_dict(todo)


@view_config(
    route_name="todos.all.xhr",
    xhr=True,
    renderer="json",
    request_method="GET")
def getTodosJSONResponse(request):
    """ get simple todo items """
    try:
        # todos = DBSession.query(Todos).order_by(Todos.id.asc()).all()
        todos = DBSession.query(Todos).order_by(Todos.id.desc()).all()
    except DBAPIError:
        return Response(
            body=json.dumps({'message': conn_err_msg}),
            content_type='application/json',
            status_int=400)
    return [
        object_to_dict(todo)
        for todo in todos
    ]


@view_config(
    route_name="todos.all.xhr",
    xhr=True,
    renderer="json",
    request_method="POST")
def createTodosJSONResponse(request):
    """ create simple todo items """
    try:
        json_body = request.json_body
    except ValueError, e:
        return Response(
            body=json.dumps({
                'message': 'No JSON object could be decoded: (%s)' % e
            }),
            status='400 Bad Request',
            content_type='application/json')

    # validation
    # check column exists
    mapper = class_mapper(Todos)
    columns = [column.key for column in mapper.columns]
    for key in list(json_body):
        if key not in columns:
            del json_body[key]

    # create model
    try:
        model = Todos(**json_body)
    except TypeError, e:
        return Response(
            body=json.dumps({
                'message': 'Failed to create model:(%s)' % e
            }),
            status='400 Bad Request',
            content_type='application/json')

    # commit transaction to get model id
    DBSession.add(model)
    DBSession.flush()

    if model.id:
        return object_to_dict(model)
        # return Response(
        #     body=json.dumps(object_to_dict(model)),
        #     content_type='application/json',
        #     status_int=201)

    return Response(
            body=json.dumps({'message': 'Failed to create model'}),
            content_type='application/json',
            status_int=400)


@view_config(
    route_name="todos.index",
    renderer="templates/classic_page.pt")
class MyClassicView(object):
    """docstring for MyClassicView"""
    def __init__(self, arg):
        super(MyClassicView, self).__init__()
        self.arg = arg

    def __call__(self):
        return {'content': 'My Todos Page'}


@view_config(
    route_name="angularjs",
    renderer="templates/angularjs.pt")
class MyAngularjsView(object):
    """docstring for MyAngularjsView"""
    def __init__(self, arg):
        super(MyAngularjsView, self).__init__()
        self.arg = arg

    def __call__(self):
        return {'content': 'My Angularjs View'}


@view_config(route_name='home', renderer='templates/mytemplate.pt')
def my_view(request):
    try:
        done = DBSession.query(Todos).filter(Todos.completed).first()
    except DBAPIError:
        return Response(conn_err_msg, content_type='text/plain', status_int=500)
    return {'done': done, 'project': 'MyProject'}

conn_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_MyProject_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""

