var AJAX_HEADER = {
    'X-Requested-With': 'XMLHttpRequest'
};
angular.module('todomvc', [
    'ngRoute',
    'restangular'
    ])
    .constant('_', window._)
    .config(function($routeProvider, RestangularProvider) {
        RestangularProvider.setDefaultHeaders(AJAX_HEADER);
        var routeConfig = {
            controller: 'TodoCtrl',
            templateUrl: 'todomvc-index.html',
            resolve: {
                store: function(todoStorage) {
                    return todoStorage
                        .then(function(module) {
                            module.get();
                            return module;
                        });
                }
            }
        };

        $routeProvider
            .when('/', routeConfig)
            .when('/:status', routeConfig)
            .otherwise({
                redirectTo: '/'
            })
    });