angular.module('todomvc')
    .controller('TodoCtrl', function ($scope, $routeParams, $filter, store) {
        var todos = $scope.todos = store.todos;
        // var todos = $scope.todos = store;

        $scope.newTodo = '';

        $scope.$watch('todos', function() {
            $scope.remainingCount = $filter('filter')(todos, {completed: false}).length;
            $scope.completedCount = todos.length - $scope.remainingCount;
            $scope.allChecked = !$scope.remainingCount;
        }, true);

        $scope.$on('$routeChangeSuccess', function() {
            var status = $scope.status = $routeParams.status || '';
            $scope.statusFilter =
                (status === store.ACTIVE_TODOS) ? { completed: false } :
                (status === store.COMPLETED_TODOS) ? { completed: true } : {};
        });

        $scope.addTodo = function() {

            var title = $scope.newTodo.trim();
            if (!title) return;

            $scope.saving = true;

            store.insert({
                    title: title,
                    completed: false
                })
                .then(function () {
                    $scope.newTodo = '';
                })
                .finally(function () {
                    $scope.saving = false;
                });
        };

        $scope.removeTodo = function(todo) {
            store.delete(todo);
        };

        $scope.toggleCompleted = function(todo) {
            store.put(todo);
        };

        $scope.editTodo = function(todo) {
            $scope.editedTodo = todo;
            $scope.originalTodo = angular.extend({}, todo);
        };

        $scope.saveEdits = function(todo) {
            store.put(todo);
        };

        $scope.clearCompletedTodos = function() {
            store.clearCompleted();
        };

        $scope.revertEdits = function (todo) {
            todos[todos.indexOf(todo)] = $scope.originalTodo;
            $scope.editedTodo = null;
            $scope.originalTodo = null;
            $scope.reverted = true;
        };
    });