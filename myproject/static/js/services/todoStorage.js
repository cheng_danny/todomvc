angular.module('todomvc')
    .factory('todoStorage', function ($http, $injector, Restangular) {
        return $http({
                    url:'/todos',
                    headers: { 'X-Requested-With': 'XMLHttpRequest' },
                    method: 'GET'
                })
                .then(
                    function () {
                        return $injector.get('api');
                    },
                    function () {
                        return $injector.get('localStorage');
                    }
                );
    })

    .factory('api', function (Restangular, $http) {
        var store = {
            ALL_TODOS: 'all',
            ACTIVE_TODOS: 'active',
            COMPLETED_TODOS: 'completed',
            todos: [],
            clearCompleted: function() {
                var originalTodos = store.todos.slice(0),
                    completeTodos = [],
                    incompleteTodos = [];

                store.todos.forEach(function(todo) {
                    if (todo.completed) {
                        completeTodos.push(todo);
                    } else {
                        incompleteTodos.push(todo);
                    }
                });

                for (var i = completeTodos.length - 1; i >= 0; i--) {
                    this.delete(completeTodos[i]);
                };
            },
            get: function() {
                return Restangular.all('todos').getList().then(
                    function(todos) {
                        angular.copy(todos, store.todos);
                        return store.todos;
                    });
            },
            insert: function (model) {
                return Restangular.all('todos').post(model).then(
                        function success(resp) {
                            return store.todos.splice(0, 0, resp);
                        },
                        function error () {
                            return store.todos;
                        }
                    );
            },
            delete: function(model) {
                var originalTodos = store.todos.slice(0);
                store.todos.splice(store.todos.indexOf(model), 1)
                return model.remove().then(
                        function success() {
                            return store.todos;
                        },
                        function error(xhr) {
                            console.log('failed to delete model %o', model);
                            return angular.copy(originalTodos, store.todos);
                        }
                    );
            },
            put: function(model) {
                var originalTodos = store.todos.slice(0);
                // Use put method on copied model
                // Due to issue on Restangular bug
                // @ref https://github.com/mgonto/restangular/issues/713
                var edited = Restangular.copy(model);
                return edited.put()
                    .then(
                        function success() {
                            // console.log(store.todos);
                            return store.todos;
                        },
                        function error() {
                            return originalTodos;
                        }
                    );
            }
        };

        return store;
    })

    .factory('localStorage', function ($q) {
        console.log('localStorage WIP....');
    });