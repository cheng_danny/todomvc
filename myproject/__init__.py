from pyramid.config import Configurator
from sqlalchemy import engine_from_config
import logging
from resource import Root

from .models import (
    DBSession,
    Base,
    )

log = logging.getLogger(__name__)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings, root_factory=Root)
    # config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('todos.all.xhr', '/todos')
    config.add_route('todos.xhr', '/todos/{todo}')
    config.add_route('todos.index', '/todo')
    config.add_route('angularjs', '/test')
    config.scan()
    return config.make_wsgi_app()
