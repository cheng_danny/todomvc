import datetime

from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    Boolean,
    DateTime
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class TimestampMixin(object):
    created_at = Column(DateTime, default=datetime.datetime.now())
    updated_at = Column(DateTime, default=datetime.datetime.now())


class Todos(Base):
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    completed = Column(Boolean)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


Index('my_index', Todos.title, unique=True, mysql_length=255)
